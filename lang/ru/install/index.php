<?
$MESS["MCART_DEPARTMENT_MODULE_NAME"]  = "Поле 'Отдел' из AD";
$MESS["MCART_DEPARTMENT_MODULE_DESC"]  = "Модуль обеспечивает функционал по перемещению сотрудника по отделам по полю 'Отдел' из AD";
$MESS["MCART_DEPARTMENT_PARTNER_NAME"] = "Эм Си Арт";
$MESS["MCART_DEPARTMENT_PARTNER_URI"]  = "https://www.mcart.ru/";

$MESS["MCART_DEPARTMENT_DENIED"] = "Доступ закрыт";
$MESS["MCART_DEPARTMENT_WRITE_SETTINGS"] = "Изменение настроек модуля";
$MESS["MCART_DEPARTMENT_FULL"] = "Полный доступ";

$MESS["MCART_DEPARTMENT_INSTALL_TITLE"] = "Установка модуля";
$MESS["MCART_DEPARTMENT_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
?>