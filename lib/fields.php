<?
namespace Mcart\Department;

use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock as HL; 
use \Bitrix\Main\Entity;
use \Bitrix\Main\EventManager;

Loader::includeModule('iblock');
Loader::includeModule("highloadblock");

class Fields 
{	
    public static $ParamsUnit;		
	public static $ParamsGroup; 
	public static $TotalNumber;
	public static $arDepartments;
	public static $countRepeatCode;

	public static function UserUpdate()
	{
		// Параметры Highload-блока	
		$hlblock = HL\HighloadBlockTable::getList(['filter' => ['=NAME' => 'LogUserUnit'], 'select' => ['*']])->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);	 
		$entity_data_class = $entity->getDataClass();
		
		// Поле сотрудника с Отделом
		self::$ParamsUnit = \COption::GetOptionString('mcart.department', 'field_line', false, false, false);
		
		// Группы исключений
		self::$ParamsGroup = \COption::GetOptionString('mcart.department', 'field_list', false, false, false);		
				
		// Находим все поразделения, и считаем количество повторяющихся кодов		
		$resDepartments = \CIBlockSection::GetList([],['IBLOCK_CODE'=>'departments', 'ACTIVE'=>'Y', '!=ID' => 1], false, ['ID', 'CODE', 'NAME'], false);		
		
		$arCode = [];
		
		while ($arDepartment = $resDepartments->GetNext()) 
		{
			$arCode[$arDepartment['ID']] = $arDepartment['CODE'];		
			
			self::$arDepartments[$arDepartment['ID']] = $arDepartment;
		}	
		
		self::$countRepeatCode = array_count_values($arCode);	
		
		
		// определение общего количества пользователей
		$obUser = \Bitrix\Main\UserTable::GetList(['filter' => ['=MAIN_USER_GROUPS_GROUP_ID' => self::$ParamsGroup, 'ACTIVE' => 'Y'], 'select' => ['GROUPS']]);
		
		$arUsers = [];
		
		while ($arUser = $obUser->Fetch()) 
		{
			$arUsers[$arUser['MAIN_USER_GROUPS_USER_ID']] = $arUser['MAIN_USER_GROUPS_GROUP_ID'];
		}
		
		$arUsers = array_keys($arUsers);
		
		self::$TotalNumber = count($arUsers);
		
		// формирование сообщения для пользователей, и запись в Highload-блок
		self::SaveHighloadblock(0, $entity_data_class, $arUsers);
		
		return '\Mcart\Department\Fields::UserUpdate();';
	}
	
	public static function SaveHighloadblock($offset, $entity_data_class, $arUsers)
	{
		// находим всех пользователей, не состоящих в заданной группе,		
		// и пользовательское поле "Поле сотрудника с Отделом"
		
		$obUser = \Bitrix\Main\UserTable::GetList([
			'filter' => ['=ID' => $arUsers],
			'select' => ['ID', 'LOGIN', 'LAST_NAME', 'NAME', 'SECOND_NAME', self::$ParamsUnit, 'UF_DEPARTMENT'],
			'offset' => $offset,
			'limit'  => ($limit = 500),
		]);	

		$user = new \CUser;		
		
		while ($arUser = $obUser->Fetch()) 
		{			
			// По коду отдела ищем его наименование или код, если нет ни одной группы с ним
			
			$resDepartments = \CIBlockSection::GetList([],['IBLOCK_CODE'=>'departments', 'ACTIVE'=>'Y', 'CODE' => $arUser[self::$ParamsUnit]], false, ['ID', 'NAME'], false);
			
			$NewGroup = [];	
			
			if ($arDepartment = $resDepartments->GetNext())
				$NewGroup = ['ID' => $arDepartment['ID']];
						
			//	Запись в лог по условиям		
			
			if (empty($arUser[self::$ParamsUnit]))
			{
				$InformationEmployee = 'Пользователь '.$arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'].' login = '.$arUser['LOGIN'].' пропущен, т.к. поле Отдел не заполнено';
			}
			elseif ( !in_array( $arUser[self::$ParamsUnit], array_keys(self::$countRepeatCode) ) )
			{
				$InformationEmployee = 'Пользователь '.$arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'].'  login = '.$arUser['LOGIN'].' пропущен, поле Отдел = "'.$arUser[self::$ParamsUnit].'", сотрудник состоит в "'.self::$arDepartments[$arUser['UF_DEPARTMENT'][0]]['NAME'].'", совпадений не найдено';				
			}
			elseif (!empty($arUser[self::$ParamsUnit]))
			{
				if (self::$countRepeatCode[$arUser[self::$ParamsUnit]] > 1)
				{
					$InformationEmployee = 'Пользователь '.$arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'].' login = '.$arUser['LOGIN'].' пропущен, поле Отдел = "'.$arUser[self::$ParamsUnit].'", нашли более 1 совпадения';					
				}
				elseif (self::$countRepeatCode[$arUser[self::$ParamsUnit]] == 1)
				{
					if (self::$arDepartments[$arUser['UF_DEPARTMENT'][0]]['CODE'] == $arUser[self::$ParamsUnit])
					{
						$InformationEmployee = 'Пользователь '.$arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'].' login = '.$arUser['LOGIN'].' пропущен, поле Отдел = "'.$arUser[self::$ParamsUnit].'", найдено 1 совпадение, сотрудник уже состоит в заданном отделе';
					}
					else
					{
						$user->Update($arUser['ID'], Array('UF_DEPARTMENT' => [$NewGroup['ID']]));
						
						$strError = $user->LAST_ERROR;

						if (isset($strError))
						{
							$InformationEmployee = 'Пользователь '.$arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'].' login = '.$arUser['LOGIN'].' перемещен в из отдела "'.self::$arDepartments[$arUser['UF_DEPARTMENT'][0]]['NAME'].'" в отдел  '.$arUser[self::$ParamsUnit];
						} 
						else 
							echo $strError;
					}
				}
			}			
			
			$data = ['UF_DATE_TIME' => date('d.m.Y H:i:s'), 'UF_INFORMATION_EMPLOYEE' => $InformationEmployee];
			$entity_data_class::add($data);
		}
		
		if (self::$TotalNumber > 0)
		{			
			--self::$TotalNumber;
			self::SaveHighloadblock(($offset + $limit), $entity_data_class, $arUsers);
		}
	}
}