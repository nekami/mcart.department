<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
Loader::includeModule('iblock');

$arGroups = [];

$result = \Bitrix\Main\GroupTable::getList([
    'select'  => ['ID', 'NAME'],
	'order'   => ["NAME"=>"ASC"]
]);

while ($arGroup = $result->fetch()) {
    $arGroups[$arGroup['ID']] = $arGroup['NAME'];
}


$module_id = 'mcart.department';

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

if ($APPLICATION->GetGroupRight($module_id)<"S")
{
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

\Bitrix\Main\Loader::includeModule($module_id);

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций

$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('MCART_DEPARTMENT_TAB_SETTINGS'),
        'OPTIONS' => array(            
            array('field_line', Loc::getMessage('MCART_DEPARTMENT_FIELD_LINE_TITLE'),
                '',
                array('text', 10)),
            array('field_list', Loc::getMessage('MCART_DEPARTMENT_FIELD_LIST_TITLE'),
                '',
                array('selectbox', $arGroups)),
        )
    ),
    /*array(
        "DIV" => "edit2",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),*/
);


#Сохранение

if ($request->isPost() && $request['Update'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption)) continue;
            if ($arOption['note']) continue;            
            $optionName = $arOption[0];
            $optionValue = $request->getPost($optionName);
            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }
}

#Визуальный вывод
$tabControl = new CAdminTabControl('tabControl', $aTabs); ?>

<? $tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='mcart_department_settings'>

    <? foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):?>
        <? $tabControl->BeginNextTab(); ?>
        <? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

    <?      endif;
        endforeach; ?>

    <? $tabControl->BeginNextTab(); ?>
	
    <? $tabControl->Buttons(); ?>

    <input type="submit" name="Update" value="<?=GetMessage('MAIN_SAVE')?>">
    <?/*<input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">*/?>
    <?=bitrix_sessid_post();?>
</form>

<div style="width: 80%; float: right;"><?=GetMessage('MCART_DEPARTMENT_COMMENT_1')?> <a href="/bitrix/admin/agent_list.php?lang=ru" target="_blank"><?=GetMessage('MCART_DEPARTMENT_COMMENT_2')?></a></div>

<? $tabControl->End(); ?>