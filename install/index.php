<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

Class mcart_department extends CModule
{
	function __construct()
	{
		$arModuleVersion = array();
		include(__DIR__."/version.php");
        $this->MODULE_ID = 'mcart.department';
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("MCART_DEPARTMENT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("MCART_DEPARTMENT_MODULE_DESC");
		$this->PARTNER_NAME = Loc::getMessage("MCART_DEPARTMENT_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("MCART_DEPARTMENT_PARTNER_URI");
	}

    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot=false)
    {
        if($notDocumentRoot)
            return str_ireplace($_SERVER["DOCUMENT_ROOT"],'',dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

    //Проверяем что система поддерживает D7
    public function isVersionD7()
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '19.00.00');
    }

    function InstallDB()
    {
        return true;
    }

    function UnInstallDB()
    {
        \Bitrix\Main\Config\Option::delete($this->MODULE_ID);
    }

	function InstallEvents()
	{
		/*
		Особенностью создания функций-агентов является то, что в качестве возвращаемого значения функция-агент должна вернуть PHP код, который будет использован при следующем запуске данной функции.
		*/			
		
        \CAgent::AddAgent('\\Mcart\\Department\\Fields::UserUpdate();', $this->MODULE_ID, "N", 3600, "", "N");
	}

	function UnInstallEvents()
	{
		\CAgent::RemoveModuleAgents($this->MODULE_ID);
	}

	function InstallFiles()
	{
        return true;
	}

	function UnInstallFiles()
	{
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
        if($this->isVersionD7())
        {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();

            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
        }
        else
        {
            $APPLICATION->ThrowException(Loc::getMessage("MCART_DEPARTMENT_INSTALL_ERROR_VERSION"));
        }
	}

	function DoUninstall()
	{
        \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UnInstallDB();
	}
	
	/*
	function GetModuleRightList()
    {
        return array(
            "reference_id" => array("D","S","W"),
            "reference" => array(
                "[D] ".Loc::getMessage("MCART_DEPARTMENT_DENIED"),                
                "[S] ".Loc::getMessage("MCART_DEPARTMENT_WRITE_SETTINGS"),
                "[W] ".Loc::getMessage("MCART_DEPARTMENT_FULL"))
        );
    }
	*/
}
?>